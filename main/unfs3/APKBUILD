# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=unfs3
pkgver=0.10.0
pkgrel=0
pkgdesc="user-space implementation of the NFSv3 server specification"
options="!check" # No testsuite
url="https://unfs3.github.io/"
arch="all"
license="BSD-3-Clause"
depends="rpcbind"
makedepends="flex-dev libtirpc-dev byacc"
subpackages="$pkgname-doc $pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://github.com/unfs3/unfs3/releases/download/unfs3-$pkgver/unfs3-$pkgver.tar.gz
	unfs3.confd
	unfs3.initd
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man
	make
}

package() {
	make DESTDIR="$pkgdir/" install
	install -D -m0644 "$srcdir"/unfs3.confd "$pkgdir"/etc/conf.d/unfs3
	install -D -m0755 "$srcdir"/unfs3.initd "$pkgdir"/etc/init.d/unfs3
}

sha512sums="
4d62b45f133a1929fc0faacfc7a483126767519332c5a311d44b1d048a6c580280299ffb70114a1c405666d730953d436957c04a3350329aa3de5256d0d3e85a  unfs3-0.10.0.tar.gz
e3a3b7a71117482fd3214eadfd957f5cd3f05da268f748f931945c390f1d8e46e19b33d53b2ce0505c61745f8dd6285f7a9de409196fedef07d23ae029f1c035  unfs3.confd
105b1ac6d0a65b3ba19bb8ada17d1ae865cf14e71e6a3838df1e6d34d2738457f0019258fd10a4594783074c10be44db75417677290bdd6862ba73b4c370b07e  unfs3.initd
"
